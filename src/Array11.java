import java.util.Scanner;
import java.util.Random;

public class Array11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[5];
        int first, second;
        for (int i = 0; i < 5; i++) {
            Random r = new Random();
            arr[i] = r.nextInt(0, 99);
        }

        System.out.println("Array");
        while (true) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();

            System.out.print("Please input index : ");
            first = sc.nextInt();
            second = sc.nextInt();

            int swap = arr[first];
            arr[first] = arr[second];
            arr[second] = swap;

            boolean isFinish = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i+1]) {
                    isFinish = false;
                }
            }
            if(isFinish){
                for (int i = 0; i < arr.length; i++) {
                    System.out.print(arr[i] + " ");
                }
                System.out.println();
                System.out.print("You winner!!!");
                break;
            }
        }
    }
}
