import java.util.Scanner;

public class Array6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] =new int[3];
        int sum =0;
        double avg;
        for(int i=0; i<arr.length;i++){
            System.out.print("Please input arr["+i+"]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for(int i=0; i<arr.length;i++){
            System.out.print(arr[i]+" ");
            sum = sum + arr[i];
        }
        avg = sum/arr.length;
        System.out.println();
        System.out.println("sum = "+ sum);
        System.out.print("avg = " + avg);
    }
    
}
