import java.util.Scanner;

public class MyApp {
    static void Welcome() {
        System.out.println("Welcome to My App");
    }

    static void Menu() {
        System.out.println("Menu");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    static int Choice() {
        int choice;
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input your choice(1-3) : ");
            choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error : Please input between 1-3");
        }
    }

    public static void main(String[] args) {
        int choice;
        while (true) {
            Welcome();
            Menu();
            choice = Choice();
            switch (choice) {
                case 1:
                    HelloWorld();
                    break;
                case 2:
                    Add();
                    break;
                case 3:
                    Exit();
                    break;
            }
        }
    }

    static void HelloWorld(){
        int count;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input count : ");
        count = sc.nextInt();
        for(int i = 0; i<count ; i++){
            System.out.println("Hello World!!!");
        }
    }
    static void Add(){
        int stnum;
        int ndnum;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number : ");
        stnum = sc.nextInt();
        System.out.print("Please input second number : ");
        ndnum = sc.nextInt();
        int sum = stnum + ndnum ;
        System.out.println("Result = " + sum);
    }
    static void Exit(){
        System.out.println("Bye!!!");
        System.exit(0);
    }
}
